#!/bin/bash
echo "First script."

find . \
-type d \
-wholename "**/node_modules" \
-not -wholename "**/node_modules/*" \
-exec rm -rf {} +
echo "Script finished."

echo "Next escript."
find . \
-name 'node_modules' \
-type d \
-prune \
-print \
-exec rm -rf {} +
echo "Script finished."
