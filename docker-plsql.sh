#!/bin/bash

echo "Baixando uma imagem oracle database:"
docker pull store/oracle/database-enterprise:12.2.0.1
echo "Criando um container:"
docker run -d -it --name oracleDB store/oracle/database-enterprise:12.2.0.1